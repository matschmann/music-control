package de.rainr.musiccontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicMachineApplication {

  public static void main(String[] args) {
    SpringApplication.run(MusicMachineApplication.class, args);
  }
}
