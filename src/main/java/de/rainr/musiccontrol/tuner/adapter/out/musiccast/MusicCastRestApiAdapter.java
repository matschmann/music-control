package de.rainr.musiccontrol.tuner.adapter.out.musiccast;

import de.rainr.musiccontrol.tuner.application.port.out.LoadRadioStationsPort;
import de.rainr.musiccontrol.tuner.application.port.out.SelectRadioStationsPort;
import de.rainr.musiccontrol.tuner.domain.Band;
import de.rainr.musiccontrol.tuner.domain.RadioStation;
import de.rainr.musiccontrol.tuner.domain.RadioStation.Id;
import java.util.List;
import java.util.Optional;

public class MusicCastRestApiAdapter implements LoadRadioStationsPort, SelectRadioStationsPort {

  @Override
  public List<RadioStation> listRadioStations(Band band) {
    return null;
  }

  @Override
  public boolean rescanRadioStations(Band band) {
    return false;
  }

  @Override
  public Optional<RadioStation> getRadioStation(Id id) {
    return Optional.empty();
  }

  @Override
  public boolean selectRadioStation(Id id) {
    return false;
  }
}
