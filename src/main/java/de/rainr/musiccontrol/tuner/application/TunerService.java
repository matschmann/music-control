package de.rainr.musiccontrol.tuner.application;

import de.rainr.musiccontrol.actualplay.application.port.in.ActualPlayUseCase;
import de.rainr.musiccontrol.tuner.application.port.in.TunerUseCase;
import de.rainr.musiccontrol.tuner.application.port.out.LoadRadioStationsPort;
import de.rainr.musiccontrol.tuner.application.port.out.SelectRadioStationsPort;
import de.rainr.musiccontrol.tuner.domain.Band;
import de.rainr.musiccontrol.tuner.domain.RadioStation;
import de.rainr.musiccontrol.tuner.domain.RadioStation.Id;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class TunerService implements TunerUseCase {

  private final LoadRadioStationsPort loadRadioStationsPort;
  private final SelectRadioStationsPort selectRadioStationsPort;
  private final ActualPlayUseCase actualPlayUseCase;

  @Override
  public boolean selectRadioStation(Id radioStationId) {
    return selectRadioStation(radioStationId);
  }

  @Override
  public List<RadioStation> listRadioStations() {
    return loadRadioStationsPort.listRadioStations(band);
  }

  @Override
  public Optional<RadioStation> getCurrentRadioStation() {
    return actualPlayUseCase.;
  }

  @Override
  public Optional<Id> getCurrentRadioStationId() {
    return Optional.empty();
  }
}
