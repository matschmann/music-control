package de.rainr.musiccontrol.tuner.application.port.in;

import de.rainr.musiccontrol.tuner.domain.RadioStation;
import de.rainr.musiccontrol.tuner.domain.RadioStation.Id;
import java.util.List;
import java.util.Optional;

public interface TunerUseCase {

  boolean selectRadioStation(Id radioStationId);
  List<RadioStation> listRadioStations();
  Optional<RadioStation> getCurrentRadioStation();
  Optional<Id> getCurrentRadioStationId();
}
