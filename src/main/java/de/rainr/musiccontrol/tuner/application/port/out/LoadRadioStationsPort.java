package de.rainr.musiccontrol.tuner.application.port.out;

import de.rainr.musiccontrol.tuner.domain.Band;
import de.rainr.musiccontrol.tuner.domain.RadioStation;
import de.rainr.musiccontrol.tuner.domain.RadioStation.Id;
import java.util.List;
import java.util.Optional;

public interface LoadRadioStationsPort {

  /**
   * Lists all available radio stations for the given band.
   *
   * @param band tuner band to scan
   * @return a list of available stations. If no station is available, an empty list is returned.
   */
  List<RadioStation> listRadioStations(Band band);

  /**
   * Drops all (cached) information about the stored radio stations and retrieves a fresh list of
   * all available stations that can be received in the current area.
   *
   * @return true, if scan was successful
   */
  boolean rescanRadioStations(Band band);

  /**
   *
   * @param id
   * @return
   */
  Optional<RadioStation> getRadioStation(Id id);

}
