package de.rainr.musiccontrol.tuner.application.port.out;

import de.rainr.musiccontrol.tuner.domain.RadioStation.Id;

public interface SelectRadioStationsPort {

  /**
   *
   * @param id
   * @return
   */
  boolean selectRadioStation(Id id);
}
