package de.rainr.musiccontrol.tuner.domain;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class RadioStation {

  @NotNull
  Id id;

  String name;

  String ensemble;

  @NotNull
  Band band;

  @Value(staticConstructor = "of")
  public static class Id {

    String value;

    @Override
    public String toString() {
      return value;
    }
  }
}
