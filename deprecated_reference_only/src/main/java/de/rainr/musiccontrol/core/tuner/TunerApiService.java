package de.rainr.musiccontrol.core.tuner;

import io.vavr.control.Either;
import java.util.List;

public interface TunerApiService {

  Either<ReceiverFailure, List<Preset>> listDabPresets();

  Either<ReceiverFailure, TunerPlayInfo> getPlayInfoResponse();

  Either<ReceiverFailure, Preset> switchToPreset(Preset preset);
}
