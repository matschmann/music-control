package de.rainr.musiccontrol.core.tuner;

import io.vavr.control.Option;
import javax.validation.constraints.Min;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Value
@Builder
public class Preset {

  @NonNull
  @Min(1)
  int position;

  @NonNull
  StationId id;

  @NonNull
  Band band;

  @With
  String stationName;

  public Option<String> getStationName() {
    return Option.of(stationName);
  }
}
