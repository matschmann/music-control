package de.rainr.musiccontrol.core.tuner;

import java.util.List;

public interface TunerService {
  List<RadioStation> listStation();

}
