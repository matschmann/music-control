package de.rainr.musiccontrol.core.tuner;

public enum Band {
  DAB, FM, AM;

  public static Band parse(String s) {
    return Band.valueOf(s.toUpperCase());
  }
}
