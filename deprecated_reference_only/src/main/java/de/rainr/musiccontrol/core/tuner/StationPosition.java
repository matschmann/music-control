package de.rainr.musiccontrol.core.tuner;

import lombok.NonNull;
import lombok.Value;

@Value( staticConstructor = "of")
public class StationPosition {

  @NonNull int position;

 @Override
  public String toString() {
   return position + "";
 }
}
