package de.rainr.musiccontrol.core.tuner;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class TunerFrequency {

  @NonNull int value;
  @NonNull Band band;
  @NonNull Preset preset;
}
