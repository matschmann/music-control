package de.rainr.musiccontrol.core.tuner;

import java.util.List;

public interface StationRepository {
  void save(List<RadioStation> radioStations);
  List<RadioStation> findAll();
}
