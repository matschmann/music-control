package de.rainr.musiccontrol.core.tuner;

import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class StationId {

  @NonNull int value;

  @Override
  public String toString() {
    return value +"";
  }
}
