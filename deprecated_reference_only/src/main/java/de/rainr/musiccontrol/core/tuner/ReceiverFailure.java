package de.rainr.musiccontrol.core.tuner;

import io.vavr.control.Option;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ReceiverFailure {

  private String message = "";
  private Option<Integer> responseCode;

  private static final Map<Integer, String> errors = new HashMap<>();

  static {
    errors.put(0, "Successful request ");
    errors.put(1, "Initializing ");
    errors.put(2, "Internal Error ");
    errors.put(3, "Invalid Request (A method did not exist, a method wasn’t appropriate etc.) ");
    errors.put(4, "Invalid Parameter (Out of range, invalid characters etc.) ");
    errors.put(5, "Guarded (Unable to setup in current status etc.) ");
    errors.put(6, "Time Out");
    errors.put(99, "Firmware Updating");

    //following are streaming service related
    errors.put(100, "Access Error");
    errors.put(101, "Other Errors");
    errors.put(102, "Wrong User Name");
    errors.put(103, "Wrong Password");
    errors.put(104, "Account Expired");
    errors.put(105, "Account Disconnected/Gone Off/Shut Down");
    errors.put(106, "Account Number Reached to the Limit");
    errors.put(107, "Server Maintenance");
    errors.put(108, "Invalid Account");
    errors.put(109, "License Error");
    errors.put(110, "Read Only Mode");
    errors.put(111, "Max Stations");
    errors.put(112, "Access Denied");
  }

  public ReceiverFailure(Option<Integer> responseCode) {
    this.responseCode = responseCode;
  }

  public ReceiverFailure(Integer responseCode) {
    this.responseCode = Option.of(responseCode);
  }

  public ReceiverFailure(String message) {
    this.message = message;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReceiverFailure that = (ReceiverFailure) o;
    return Objects.equals(responseCode, that.responseCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(responseCode);
  }

  @Override
  public String toString() {
    if (responseCode.isEmpty()) {
      return "Error (no response code), message=" + message;
    } else {
      return String.format("Error %s: %s", responseCode, errors.get(responseCode.get()));
    }
  }
}
