package de.rainr.musiccontrol.core.tuner;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ReceiverTunerService implements TunerService {

  private static final Logger LOG = getLogger(ReceiverTunerService.class);
  private final TunerApiService tunerApiService;
  private final StationRepository stationRepository;

  public ReceiverTunerService(TunerApiService tunerApiService,
      StationRepository stationRepository) {
    this.tunerApiService = tunerApiService;
    this.stationRepository = stationRepository;
  }

  public List<RadioStation> listStation() {
    var storedStations = stationRepository.findAll();
    if (storedStations.isEmpty()) {
      var stations = getStationsFromApi();
      stationRepository.save(stations);
      return stations;
    } else {
      return storedStations;
    }

  }

  private List<RadioStation> getStationsFromApi() {
    return tunerApiService.listDabPresets()
        .peekLeft(it -> LOG.error("Error retrieving list of radio presets: {}", it))
        .map(presets -> presets.stream()
            .map(this::toRadioStation)
            .collect(Collectors.toList()))
        .getOrElse(Collections::emptyList);
  }

  private RadioStation toRadioStation(Preset preset) {
    return RadioStation.builder()
        .id(preset.getId().getValue() + "")
        .name(preset.getStationName().getOrElse("unknown"))
        .positionOnReceiver(StationPosition.of(preset.getPosition()))
        .build();
  }

}
