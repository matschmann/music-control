package de.rainr.musiccontrol.core.tuner;


import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class RadioStation {

  @NonNull String id;
  @NonNull StationPosition positionOnReceiver;
  @NonNull String name;

}
