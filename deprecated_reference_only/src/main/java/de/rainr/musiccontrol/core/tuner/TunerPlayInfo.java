package de.rainr.musiccontrol.core.tuner;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(access = AccessLevel.PUBLIC)
public class TunerPlayInfo {

  @NonNull AudioMode audioMode;
  int bitRate;
  @NonNull TunerFrequency frequency;
  @NonNull String dynamicLabelSegment;
  @NonNull String ensembleLabel;
  @NonNull String serviceLabel;
}
