package de.rainr.musiccontrol.core.tuner;

public enum AudioMode {
  MONO, STEREO;

  public static AudioMode parse(String audioMode) {
    return AudioMode.valueOf(audioMode.toUpperCase());
  }
}
