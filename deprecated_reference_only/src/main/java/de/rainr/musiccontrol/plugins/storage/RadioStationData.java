package de.rainr.musiccontrol.plugins.storage;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(access = AccessLevel.PACKAGE)
class RadioStationData {
  String id;
  int positionOnReceiver;
  String name;

}
