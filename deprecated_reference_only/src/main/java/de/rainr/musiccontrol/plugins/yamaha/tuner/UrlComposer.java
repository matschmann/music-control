package de.rainr.musiccontrol.plugins.yamaha.tuner;

import java.net.URI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
class UrlComposer {

  private final String apiUrl;

  public UrlComposer(
      @Value("${yamaha.base-url}") String baseUrl,
      @Value("${yamaha.api-prefix}") String apiPrefix) {
    this.apiUrl = baseUrl + apiPrefix;
  }

  String presetInfo() {
    return URI.create(apiUrl + "tuner/getPresetInfo?band=dab").toString();
  }

  String playInfo() {
    return URI.create(apiUrl + "tuner/getPlayInfo").toString();
  }

  String recallPreset(int presetNo) {
    return URI.create(apiUrl + "tuner/recallPreset?zone=main&band=dab&num=" + presetNo).toString();
  }
}
