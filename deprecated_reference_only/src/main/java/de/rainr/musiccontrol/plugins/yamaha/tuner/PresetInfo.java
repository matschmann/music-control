package de.rainr.musiccontrol.plugins.yamaha.tuner;


import lombok.Data;

@Data
class PresetInfo {
  String band;
  int number;
}
