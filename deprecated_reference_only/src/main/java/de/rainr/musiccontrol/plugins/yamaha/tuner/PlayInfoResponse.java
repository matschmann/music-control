package de.rainr.musiccontrol.plugins.yamaha.tuner;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
class PlayInfoResponse extends YamahaApiResponse {

  String band;
  @JsonAlias("auto_scan")
  boolean autoScan;
  DabPlayInfoResponse dab;

  @Data
  @EqualsAndHashCode(callSuper=false)
  class DabPlayInfoResponse extends YamahaApiResponse {

    int id;
    @JsonAlias("audio_mode")
    String audioMode;
    @JsonAlias("bit_rate")
    int bitRate;
    @JsonAlias("ensemble_label")
    String ensembleLabel;
    @JsonAlias("service_label")
    String serviceLabel;
    String frequency;
    @JsonAlias("dls")
    String dynamicLabelSegment;
    int preset;
  }
}
