package de.rainr.musiccontrol.plugins.storage;

import static org.slf4j.LoggerFactory.getLogger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.rainr.musiccontrol.core.tuner.RadioStation;
import de.rainr.musiccontrol.core.tuner.StationPosition;
import de.rainr.musiccontrol.core.tuner.StationRepository;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
class FileStationRepository implements StationRepository {

  private static final Logger LOG = getLogger(FileStationRepository.class);
  static final String FILE_NAME = "radiostations.json";

  private final Path storage;

  public FileStationRepository(@Value("${tuner.station-repository}") Path storage) {
    Path of = Path.of(storage.toString(), FILE_NAME);
    if (!Files.exists(of)) {
      try {
        Files.createDirectories(storage);
        Files.createFile(of);
      } catch (IOException e) {
        throw new IllegalArgumentException("Cannot create file for storing radio stations.", e);
      }
    }

    if (!Files.isRegularFile(of)) {
      throw new IllegalArgumentException("tuner.station-repository  must be a file");
    }
    if (!Files.isReadable(of)) {
      throw new IllegalArgumentException("tuner.station-repository must be a readable");
    }
    if (!Files.isWritable(of)) {
      throw new IllegalArgumentException("tuner.station-repository must be a writable");
    }

    this.storage = of;
  }

  @Override
  public void save(List<RadioStation> radioStations) {
    List<RadioStationData> dataRatioStations = radioStations.stream()
        .map(it -> RadioStationData.builder().id(it.getId()).name(it.getName())
            .positionOnReceiver(it.getPositionOnReceiver().getPosition()).build())
        .collect(Collectors.toList());

    ObjectMapper objectMapper = new ObjectMapper();
    try {
      objectMapper.writeValue(storage.toFile(), dataRatioStations);
    } catch (IOException e) {
      LOG.error("Can't store radio stations to file:{}", e.getMessage(), e);
    }
  }

  @Override
  public List<RadioStation> findAll() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      String s = Files.readString(storage);
      List<RadioStationData> asList =
          objectMapper.readValue(
              s, new TypeReference<List<RadioStationData>>() {
              });

      return asList.stream()
          .map(it -> RadioStation.builder().id(it.getId()).name(it.getName()).positionOnReceiver(
              StationPosition.of(it.positionOnReceiver)).build())
          .collect(Collectors.toList());
    } catch (IOException e) {
      LOG.error("Can't load radio stations from file:{}", e.getMessage(), e);
      return Collections.emptyList();
    }
  }
}
