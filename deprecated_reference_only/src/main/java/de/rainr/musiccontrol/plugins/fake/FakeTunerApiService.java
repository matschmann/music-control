package de.rainr.musiccontrol.plugins.fake;

import de.rainr.musiccontrol.core.tuner.AudioMode;
import de.rainr.musiccontrol.core.tuner.Band;
import de.rainr.musiccontrol.core.tuner.Preset;
import de.rainr.musiccontrol.core.tuner.ReceiverFailure;
import de.rainr.musiccontrol.core.tuner.StationId;
import de.rainr.musiccontrol.core.tuner.TunerApiService;
import de.rainr.musiccontrol.core.tuner.TunerFrequency;
import de.rainr.musiccontrol.core.tuner.TunerPlayInfo;
import io.vavr.control.Either;
import java.util.List;
import java.util.Random;
import org.springframework.context.annotation.Profile;

@Profile("fake")
public class FakeTunerApiService implements TunerApiService {

  private List<Preset> presets = List.of(
      Preset.builder().position(1).stationName("hr1").id(StationId.of(21)).band(Band.DAB).build(),
      Preset.builder().position(2).stationName("hr2").id(StationId.of(28)).band(Band.DAB).build(),
      Preset.builder().position(3).stationName("hr3").id(StationId.of(35)).band(Band.DAB).build(),
      Preset.builder().position(4).stationName("hr4").id(StationId.of(39)).band(Band.DAB).build(),
      Preset.builder().position(5).stationName("hr-xxl").id(StationId.of(2)).band(Band.DAB).build(),
      Preset.builder().position(6).stationName("hr0").id(StationId.of(89)).band(Band.DAB).build()
  );

  private Preset currentPreset;

  public FakeTunerApiService() {
    this.currentPreset = presets.get(new Random().nextInt(presets.size()));
  }

  public FakeTunerApiService(List<Preset> presets) {
    this.presets = presets;
  }

  @Override
  public Either<ReceiverFailure, List<Preset>> listDabPresets() {
    return Either.right(presets);
  }

  @Override
  public Either<ReceiverFailure, TunerPlayInfo> getPlayInfoResponse() {
    return Either.right(
        TunerPlayInfo.builder()
            .audioMode(AudioMode.STEREO)
            .bitRate(122)
            .dynamicLabelSegment("Foo bar dls " + currentPreset.getId())
            .ensembleLabel("hessischer rundfunk " + currentPreset.getId())
            .frequency(
                TunerFrequency.builder().band(Band.DAB).preset(currentPreset).value(893).build())
            .serviceLabel("hr" + currentPreset.getPosition())
            .build());
  }

  @Override
  public Either<ReceiverFailure, Preset> switchToPreset(Preset preset) {
    if (presets.contains(preset)) {
      currentPreset = preset;
      return Either.right(preset);
    } else {
      return Either.left(new ReceiverFailure(4));
    }
  }

  public List<Preset> getPresets() {
    return presets;
  }
}
