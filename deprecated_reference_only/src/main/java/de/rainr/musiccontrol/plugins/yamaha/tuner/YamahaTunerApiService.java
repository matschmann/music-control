package de.rainr.musiccontrol.plugins.yamaha.tuner;

import de.rainr.musiccontrol.core.tuner.AudioMode;
import de.rainr.musiccontrol.core.tuner.Band;
import de.rainr.musiccontrol.core.tuner.Preset;
import de.rainr.musiccontrol.core.tuner.ReceiverFailure;
import de.rainr.musiccontrol.core.tuner.StationId;
import de.rainr.musiccontrol.core.tuner.TunerApiService;
import de.rainr.musiccontrol.core.tuner.TunerFrequency;
import de.rainr.musiccontrol.core.tuner.TunerPlayInfo;
import io.vavr.control.Either;
import io.vavr.control.Option;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class YamahaTunerApiService implements TunerApiService {

  private final RestTemplate restTemplate;
  private final UrlComposer urlComposer;

  YamahaTunerApiService(RestTemplate restTemplate, UrlComposer urlComposer) {
    this.restTemplate = restTemplate;
    this.urlComposer = urlComposer;

  }

  @Override
  public Either<ReceiverFailure, List<Preset>> listDabPresets() {
    final var response = restTemplate
        .getForEntity(urlComposer.presetInfo(), ApiPresetInfos.class);

    Option<Integer> responseCode = Option.of(response.getBody())
        .map(YamahaApiResponse::getResponseCode);
    if (isErrorCode(responseCode)) {
      return Either.left(new ReceiverFailure(responseCode));
    }

    var presetInfos = response.getBody().getPresetInfos();

    var presets = new ArrayList<Preset>();
    for (int presetCounter = 0; presetCounter < presetInfos.size(); presetCounter++) {
      PresetInfo presetInfo = presetInfos.get(presetCounter);
      if (!presetInfo.getBand().equals("unknown")) {
        presets.add(
            Preset.builder()
                .position(presetCounter + 1)
                .id(StationId.of(presetInfo.getNumber()))
                .band(Band.parse(presetInfo.getBand()))
                .build());
      }
    }
    return Either.right(presets);
  }

  private boolean isErrorCode(Option<Integer> responseCode) {
    return responseCode.isEmpty() || responseCode.get() != 0;
  }

  @Override
  public Either<ReceiverFailure, TunerPlayInfo> getPlayInfoResponse() {
    final var response = restTemplate
        .getForEntity(urlComposer.playInfo(), PlayInfoResponse.class);

    var body = response.getBody();
    assert body != null;

    return Either.right(TunerPlayInfo.builder()
        .audioMode(AudioMode.parse(body.getDab().getAudioMode()))
        .bitRate(body.getDab().getBitRate())
        .dynamicLabelSegment(body.getDab().getDynamicLabelSegment())
        .ensembleLabel(body.getDab().getEnsembleLabel())
        .frequency(TunerFrequency.builder()
            .band(Band.parse(body.getBand()))
            .preset(
                Preset.builder()
                    .band(Band.parse(body.getBand()))
                    .id(StationId.of(body.getDab().getId()))
                    .position(body.getDab().getPreset())
                    .build())
            .build())
        .serviceLabel(body.getDab().getServiceLabel())
        .build());

  }

  @Override
  public Either<ReceiverFailure, Preset> switchToPreset(Preset preset) {
    return switchToPreset(preset.getPosition()).map(it -> preset);
  }

  private Either<ReceiverFailure, Integer> switchToPreset(int presetPosition) {
    final var response = restTemplate
        .getForEntity(urlComposer.recallPreset(presetPosition), YamahaApiResponse.class);

    Option<Integer> responseCode = Option.of(response.getBody())
        .map(YamahaApiResponse::getResponseCode);
    if (isErrorCode(responseCode)) {
      return Either.left(new ReceiverFailure(responseCode));
    }

    return Either.right(presetPosition);
  }
}
