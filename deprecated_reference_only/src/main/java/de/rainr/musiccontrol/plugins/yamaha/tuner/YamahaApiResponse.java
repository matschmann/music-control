package de.rainr.musiccontrol.plugins.yamaha.tuner;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;

@Getter
class YamahaApiResponse {

  @JsonAlias("response_code")
  int responseCode;


}
