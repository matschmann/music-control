package de.rainr.musiccontrol.plugins.classic_webapp.radio;

import de.rainr.musiccontrol.core.tuner.TunerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class RadioController {

  private final TunerService tunerService;

  RadioController(TunerService tunerService) {
    this.tunerService = tunerService;
  }

  @GetMapping("/radio")
  public String showRadioStations(Model model) {
    model.addAttribute("stations", tunerService.listStation());
    return "radio-stations";
  }
}