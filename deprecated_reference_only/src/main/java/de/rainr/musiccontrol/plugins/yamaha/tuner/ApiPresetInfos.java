package de.rainr.musiccontrol.plugins.yamaha.tuner;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
class ApiPresetInfos extends YamahaApiResponse {
  @JsonAlias("preset_info")
  List<PresetInfo> presetInfos;

}
