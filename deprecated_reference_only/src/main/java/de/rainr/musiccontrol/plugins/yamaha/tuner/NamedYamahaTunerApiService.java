package de.rainr.musiccontrol.plugins.yamaha.tuner;

import static org.slf4j.LoggerFactory.getLogger;

import de.rainr.musiccontrol.core.tuner.Preset;
import de.rainr.musiccontrol.core.tuner.ReceiverFailure;
import de.rainr.musiccontrol.core.tuner.TunerApiService;
import de.rainr.musiccontrol.core.tuner.TunerPlayInfo;
import io.vavr.control.Either;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * The Yamaha radio unit does not provide radio station names when getting the list of presets.
 * To get the station names, this service selects each presets, waits one second and retrieves the
 * station name from the play info.
 * This takes some time and turns of the current music.
 */
@Primary
@Service
class NamedYamahaTunerApiService implements TunerApiService {
  private static final Logger LOG = getLogger(NamedYamahaTunerApiService.class);

  private final TunerApiService service;
  private long threadSleepMillis;


  NamedYamahaTunerApiService(TunerApiService service,
      @Value("${yamaha.tuner.station-change-timeout:1000}") String threadSleepMillis) {
    this.service = service;
    this.threadSleepMillis = Long.parseLong(threadSleepMillis);
  }

  @Override
  public Either<ReceiverFailure, List<Preset>> listDabPresets() {
    //TODO turn on the radio first (api call is not yet implemented)
    Either<ReceiverFailure, List<Preset>> lists = service.listDabPresets();
    List<Preset> objects = lists.get().stream()
        .map(it -> it.withStationName(getName(it)))
        .peek(it->LOG.info("Found radio preset {}", it))
        .collect(Collectors.toUnmodifiableList());
    return Either.right(objects);
  }


  private String getName(Preset preset) {
    service.switchToPreset(preset);
    try {
      Thread.sleep(threadSleepMillis);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      throw new RuntimeException("Thread interrupted", e);
    }
    return service.getPlayInfoResponse().get().getServiceLabel();
  }

  @Override
  public Either<ReceiverFailure, TunerPlayInfo> getPlayInfoResponse() {
    return service.getPlayInfoResponse();
  }

  @Override
  public Either<ReceiverFailure, Preset> switchToPreset(Preset preset) {
    return service.switchToPreset(preset);
  }

}
