package de.rainr.musiccontrol.plugins.storage;

import static de.rainr.musiccontrol.plugins.storage.FileStationRepository.FILE_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import de.rainr.musiccontrol.core.tuner.RadioStation;
import de.rainr.musiccontrol.core.tuner.StationPosition;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;

class FileStationRepositoryTest {

  @Test
  void store_location_must_be_readable() throws Exception {
    Path file = Files.createTempFile("foo", "bar", noReadyPermissions());

    assertThatThrownBy(() -> new FileStationRepository(file))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void store_location_must_be_writable() throws Exception {
    Path file = Files.createTempFile("foo", "bar", noWritePermissions());

    assertThatThrownBy(() -> new FileStationRepository(file))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private FileAttribute<Set<PosixFilePermission>> noWritePermissions() {
    return PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("r--r--r--"));
  }

  private FileAttribute<Set<PosixFilePermission>> noReadyPermissions() {
    return PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("---------"));
  }

  private FileAttribute<Set<PosixFilePermission>> readWritePermissions() {
    return PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxrwxrw-"));
  }


  @Test
  void store_entries() throws Exception {
    Path dir = Files.createTempDirectory("foo");
    FileStationRepository fileStationRepository = new FileStationRepository(dir);

    //act
    fileStationRepository.save(stations());

    //assert
    String fileContent = Files
        .readString(Path.of(dir.toString(), FILE_NAME), StandardCharsets.UTF_8);
    System.out.println(fileContent);
    assertThat(fileContent)
        .contains("id-foo")
        .contains("id-bar");
  }

  @Test
  void load_entries() throws Exception {
    Path dir = Files.createTempDirectory("foo");
    FileStationRepository fileStationRepository = new FileStationRepository(dir);
    fileStationRepository.save(stations());

    //act
    List<RadioStation> stations = fileStationRepository.findAll();

    //assert
    assertThat(stations).containsAll(stations());
  }

  @Test
  void if_file_does_not_exist_it_should_be_created() throws Exception {
    Path dir = Files.createTempDirectory("foo");

    new FileStationRepository(dir);

    assertThat(Files.list(dir)).contains(Path.of(dir.toString(), FILE_NAME));

  }

  private List<RadioStation> stations() {
    return List.of(RadioStation.builder()
            .positionOnReceiver(StationPosition.of(1))
            .name("foo")
            .id("id-foo")
            .build(),
        RadioStation.builder()
            .positionOnReceiver(StationPosition.of(1))
            .name("bar")
            .id("id-bar")
            .build());
  }

}