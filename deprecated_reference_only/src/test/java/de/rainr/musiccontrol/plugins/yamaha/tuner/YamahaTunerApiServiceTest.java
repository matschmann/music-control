package de.rainr.musiccontrol.plugins.yamaha.tuner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.anything;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.queryParam;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import de.rainr.musiccontrol.core.tuner.AudioMode;
import de.rainr.musiccontrol.core.tuner.Band;
import de.rainr.musiccontrol.core.tuner.Preset;
import de.rainr.musiccontrol.core.tuner.ReceiverFailure;
import de.rainr.musiccontrol.core.tuner.StationId;
import de.rainr.musiccontrol.core.tuner.TunerFrequency;
import io.vavr.control.Option;
import org.assertj.core.api.SoftAssertions;
import org.assertj.vavr.api.VavrAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

class YamahaTunerApiServiceTest {

  private static final MockUrlComposer URL_COMPOSER = new MockUrlComposer();
  private MockRestServiceServer mockServer;
  private YamahaTunerApiService tunerApiService;


  @BeforeEach
  void setUp() {
    RestTemplate restTemplate = new RestTemplate();
    mockServer = MockRestServiceServer.bindTo(restTemplate).build();
    tunerApiService = new YamahaTunerApiService(restTemplate, URL_COMPOSER);
  }

  @Test
  void list_presets() {
    //arrange
    mockServer.expect(requestTo(URL_COMPOSER.presetInfo()))
        .andExpect(queryParam("band", "dab"))
        .andRespond(withSuccess()
            .contentType(MediaType.APPLICATION_JSON)
            .body(presetsResponse()));

    mockServer.expect(ExpectedCount.manyTimes(), anything());

    //act
    var presets = tunerApiService.listDabPresets().get();

    //assert
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(presets.get(0).getId()).isEqualTo(StationId.of(33));
    soft.assertThat(presets.get(0).getPosition()).isEqualTo(1);
    soft.assertThat(presets.get(0).getBand()).isEqualTo(Band.DAB);

    soft.assertThat(presets.get(1).getId()).isEqualTo(StationId.of(27));
    soft.assertThat(presets.get(1).getPosition()).isEqualTo(2);
    soft.assertThat(presets.get(1).getBand()).isEqualTo(Band.DAB);
    soft.assertAll();
  }

  @Test
  void body_with_error_code() {
    //arrange
    mockServer.expect(requestTo(URL_COMPOSER.presetInfo()))
        .andRespond(withSuccess()
            .contentType(MediaType.APPLICATION_JSON)
            .body(responseWithCode(4)));

    //act
    var response = tunerApiService.listDabPresets();

    //assert
    VavrAssertions.assertThat(response).isLeft().containsOnLeft(new ReceiverFailure(Option.of(4)));
  }

  @Test
  void null_body() {
    //arrange
    mockServer.expect(requestTo(URL_COMPOSER.presetInfo()))
        .andRespond(withSuccess()
            .contentType(MediaType.APPLICATION_JSON));

    //act
    var response = tunerApiService.listDabPresets();

    //assert
    VavrAssertions.assertThat(response).isLeft().containsOnLeft(new ReceiverFailure(Option.none()));
  }


  @Test
  void switch_to_existing_preset() {
    mockServer.expect(requestTo(URL_COMPOSER.recallPreset(1)))
        .andRespond(withSuccess()
            .contentType(MediaType.APPLICATION_JSON)
            .body(emptySuccessResponse()));

    var either = tunerApiService
        .switchToPreset(Preset.builder().position(1).id(StationId.of(231)).band(Band.DAB).build());

    VavrAssertions.assertThat(either).isRight();
  }

  @Test
  void switch_to_non_existing_preset_should_fail() {
    mockServer.expect(requestTo(URL_COMPOSER.recallPreset(223)))
        .andRespond(withSuccess()
            .contentType(MediaType.APPLICATION_JSON)
            .body(responseWithCode(4)));

    var either = tunerApiService
        .switchToPreset(
            Preset.builder().position(223).id(StationId.of(231)).band(Band.DAB).build());

    VavrAssertions.assertThat(either).isLeft();
  }

  @Test
  void playResponse_contains_values() {
    //arrange
    mockServer.expect(requestTo(URL_COMPOSER.playInfo()))
        .andRespond(withSuccess()
            .contentType(MediaType.APPLICATION_JSON)
            .body(playInfoResponse()));

    //act
    var either = tunerApiService.getPlayInfoResponse();

    //assert
    VavrAssertions.assertThat(either).isRight();
    assertThat(either.get().getAudioMode()).isEqualTo(AudioMode.STEREO);
    assertThat(either.get().getBitRate()).isEqualTo(136);
    assertThat(either.get().getDynamicLabelSegment()).isEqualTo("hr3 - Die mit den Lieblingssongs");
    assertThat(either.get().getEnsembleLabel()).isEqualTo("hr Radio");
    assertThat(either.get().getServiceLabel()).isEqualTo("hr3");
    assertThat(either.get().getFrequency()).isEqualTo(
        TunerFrequency.builder()
            .band(Band.DAB)
            .preset(Preset.builder()
                .position(3)
                .id(StationId.of(27))
                .band(Band.DAB)
                .build())
            .build());
  }


  private String emptySuccessResponse() {
    return responseWithCode(0);
  }

  private String responseWithCode(int code) {
    return "{\n"
        + "    \"response_code\": " + code + "\n"
        + "}";
  }


  private String presetsResponse() {
    return "{\n"
        + "    \"func_list\": [\n"
        + "        \"clear\",\n"
        + "        \"move\"\n"
        + "    ],\n"
        + "    \"preset_info\": [\n"
        + "        {\n"
        + "            \"band\": \"dab\",\n"
        + "            \"hd_program\": 0,\n"
        + "            \"number\": 33,\n"
        + "            \"text\": \"\"\n"
        + "        },\n"
        + "        {\n"
        + "            \"band\": \"dab\",\n"
        + "            \"hd_program\": 0,\n"
        + "            \"number\": 27,\n"
        + "            \"text\": \"\"\n"
        + "        },\n"
        + "        {\n"
        + "            \"band\": \"unknown\",\n"
        + "            \"hd_program\": 0,\n"
        + "            \"number\": 0,\n"
        + "            \"text\": \"\"\n"
        + "        }\n"
        + "    ],\n"
        + "    \"response_code\": 0\n"
        + "}";
  }

  private String playInfoResponse() {
    return "{\n"
        + "    \"auto_scan\": false,\n"
        + "    \"band\": \"dab\",\n"
        + "    \"dab\": {\n"
        + "        \"audio_mode\": \"stereo\",\n"
        + "        \"bit_rate\": 136,\n"
        + "        \"category\": \"primary\",\n"
        + "        \"ch_label\": \" 7B\",\n"
        + "        \"dab_plus\": true,\n"
        + "        \"dls\": \"hr3 - Die mit den Lieblingssongs\",\n"
        + "        \"ensemble_label\": \"hr Radio\",\n"
        + "        \"freq\": 190640,\n"
        + "        \"id\": 27,\n"
        + "        \"off_air\": false,\n"
        + "        \"preset\": 3,\n"
        + "        \"program_type\": \"Classics\",\n"
        + "        \"quality\": 100,\n"
        + "        \"service_label\": \"hr3\",\n"
        + "        \"status\": \"ready\",\n"
        + "        \"tune_aid\": 93\n"
        + "    },\n"
        + "    \"fm\": {\n"
        + "        \"audio_mode\": \"mono\",\n"
        + "        \"freq\": 87500,\n"
        + "        \"preset\": 0,\n"
        + "        \"tuned\": false\n"
        + "    },\n"
        + "    \"rds\": {\n"
        + "        \"program_service\": \"\",\n"
        + "        \"program_type\": \"\",\n"
        + "        \"radio_text_a\": \"\",\n"
        + "        \"radio_text_b\": \"\"\n"
        + "    },\n"
        + "    \"response_code\": 0\n"
        + "}";
  }
}