package de.rainr.musiccontrol.plugins.classic_webapp.radio;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.rainr.musiccontrol.core.tuner.MockStationRepository;
import de.rainr.musiccontrol.core.tuner.RadioStation;
import de.rainr.musiccontrol.core.tuner.ReceiverFailure;
import de.rainr.musiccontrol.core.tuner.ReceiverTunerService;
import de.rainr.musiccontrol.core.tuner.StationPosition;
import de.rainr.musiccontrol.core.tuner.TunerApiService;
import de.rainr.musiccontrol.plugins.fake.FakeTunerApiService;
import io.vavr.control.Either;
import java.util.List;
import org.junit.jupiter.api.Test;

class ReceiverTunerServiceTest {

  @Test
  void there_are_radio_stations() {
    FakeTunerApiService tunerApiService = new FakeTunerApiService();
    ReceiverTunerService receiverTunerService =
        new ReceiverTunerService(tunerApiService, MockStationRepository.empty());

    List<RadioStation> list = receiverTunerService.listStation();

    assertThat(list).hasSameSizeAs(tunerApiService.getPresets());
  }

  @Test
  void when_stations_are_cached_locally_then_dont_call_api_service() {
    //arrange
    TunerApiService tunerApiService = spy(FakeTunerApiService.class);
    List<RadioStation> radioStations = createRadioStations();
    ReceiverTunerService receiverTunerService =
        new ReceiverTunerService(tunerApiService, MockStationRepository.having(radioStations));

    //act
    List<RadioStation> list = receiverTunerService.listStation();

    //assert
    verifyZeroInteractions(tunerApiService);
    assertThat(list).hasSameSizeAs(radioStations);
  }

  private List<RadioStation> createRadioStations() {
    return List.of(
        RadioStation.builder()
            .positionOnReceiver(StationPosition.of(1)).name("hr1").id("15").build(),
        RadioStation.builder()
            .positionOnReceiver(StationPosition.of(2)).name("hr2").id("18").build(),
        RadioStation.builder()
            .positionOnReceiver(StationPosition.of(3)).name("hr3").id("29").build()
        );
  }


  @Test
  void radio_stations_are_having_a_name_when_retrieving_from_api() {
    FakeTunerApiService tunerApiService = new FakeTunerApiService();
    ReceiverTunerService receiverTunerService =
        new ReceiverTunerService(tunerApiService, MockStationRepository.empty());

    List<RadioStation> list = receiverTunerService.listStation();

    assertThat(list).extracting(RadioStation::getName)
        .contains("hr1", "hr2" , "hr3", "hr4");
  }

  @Test
  void empty_radio_station_list_when_error_occurred() {
    TunerApiService tunerApiService = mock(TunerApiService.class);
    when(tunerApiService.listDabPresets()).thenReturn(Either.left(new ReceiverFailure(5)));
    ReceiverTunerService receiverTunerService =
        new ReceiverTunerService(tunerApiService, MockStationRepository.empty());

    List<RadioStation> list = receiverTunerService.listStation();

    assertThat(list).isEmpty();
  }

}