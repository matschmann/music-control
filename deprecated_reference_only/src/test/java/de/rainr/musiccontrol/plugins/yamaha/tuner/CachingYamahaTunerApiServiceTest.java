package de.rainr.musiccontrol.plugins.yamaha.tuner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.rainr.musiccontrol.core.tuner.AudioMode;
import de.rainr.musiccontrol.core.tuner.Band;
import de.rainr.musiccontrol.core.tuner.Preset;
import de.rainr.musiccontrol.core.tuner.ReceiverFailure;
import de.rainr.musiccontrol.core.tuner.StationId;
import de.rainr.musiccontrol.core.tuner.TunerApiService;
import de.rainr.musiccontrol.core.tuner.TunerFrequency;
import de.rainr.musiccontrol.core.tuner.TunerPlayInfo;
import io.vavr.control.Either;
import io.vavr.control.Option;
import java.util.List;
import org.junit.jupiter.api.Test;

class CachingYamahaTunerApiServiceTest {

  private static final String RADIO_STATION_NAME = "Radio Foo";

  @Test
  void listDabPresets_has_a_name_defined() {
    TunerApiService mock = mock(TunerApiService.class);
    when(mock.listDabPresets()).thenReturn(Either.right(presetWithoutName()));
    when(mock.getPlayInfoResponse()).thenReturn(Either.right(playInfoWithStationName()));

    NamedYamahaTunerApiService service = new NamedYamahaTunerApiService(mock, "0");

    //act
    Either<ReceiverFailure, List<Preset>> lists = service.listDabPresets();

    //assert
    assertThat(lists.get())
        .extracting(Preset::getStationName)
        .extracting(Option::get)
        .containsOnly(RADIO_STATION_NAME);
  }

  private TunerPlayInfo playInfoWithStationName() {

    return TunerPlayInfo.builder()
        .audioMode(AudioMode.STEREO)
        .bitRate(22)
        .dynamicLabelSegment("asdasdas")
        .ensembleLabel("adfsdfsdf")
        .frequency(TunerFrequency.builder().band(Band.DAB)
            .preset(Preset.builder().band(Band.DAB).id(StationId.of(3)).position(3).build())
            .build())
        .serviceLabel(RADIO_STATION_NAME)
        .build();
  }

  private List<Preset> presetWithoutName() {
    return List.of(Preset.builder().position(1).id(
        StationId.of(232)).band(Band.DAB).build());
  }
}