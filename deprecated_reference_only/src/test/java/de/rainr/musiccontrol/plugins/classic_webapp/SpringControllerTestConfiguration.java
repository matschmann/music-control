package de.rainr.musiccontrol.plugins.classic_webapp;

import de.rainr.musiccontrol.core.tuner.MockStationRepository;
import de.rainr.musiccontrol.core.tuner.MockTunerService;
import de.rainr.musiccontrol.core.tuner.StationRepository;
import de.rainr.musiccontrol.core.tuner.TunerService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackages = {"de.rainr.musiccontrol.plugins.classic_webapp.radio"})
public class SpringControllerTestConfiguration {

  @Bean
  TunerService tunerService() {
    return new MockTunerService();
  }

  @Bean
  StationRepository stationRepository() {
    return MockStationRepository.empty();
  }

//  @Bean
//  public RestTemplate restTemplate() {
//    return new RestTemplate();
//  }

}