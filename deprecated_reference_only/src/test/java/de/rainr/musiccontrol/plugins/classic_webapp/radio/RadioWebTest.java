package de.rainr.musiccontrol.plugins.classic_webapp.radio;


import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import de.rainr.musiccontrol.TestTags;
import de.rainr.musiccontrol.plugins.classic_webapp.SpringControllerTestConfiguration;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = {RadioController.class})
@ContextConfiguration(classes = SpringControllerTestConfiguration.class)
@Tag(TestTags.LONG_RUNNING)
class RadioWebTest {

  @Autowired
  MockMvc mockMvc;

  @Test
  void website_shows_stations() throws Exception {
    mockMvc.perform(get("/radio"))
        .andExpect(status().isOk())
        .andExpect(view().name("radio-stations"))
        .andExpect(content().string(containsString("hr-xxl")));
  }
}