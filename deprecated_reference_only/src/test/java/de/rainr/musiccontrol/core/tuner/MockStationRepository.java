package de.rainr.musiccontrol.core.tuner;

import java.util.ArrayList;
import java.util.List;

public class MockStationRepository implements StationRepository {

  private final List<RadioStation> stations;

  private MockStationRepository(List<RadioStation> stations) {
    this.stations = stations;
  }

  public static MockStationRepository having(List<RadioStation> radioStation) {
    return new MockStationRepository(new ArrayList<>(radioStation));
  }

  public static MockStationRepository empty() {
    return new MockStationRepository(new ArrayList<>());
  }

  @Override
  public void save(List<RadioStation> radioStations) {
    this.stations.addAll(radioStations);
  }

  @Override
  public List<RadioStation> findAll() {
    return stations;
  }
}
