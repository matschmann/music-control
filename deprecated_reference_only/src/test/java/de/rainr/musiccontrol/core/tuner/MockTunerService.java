package de.rainr.musiccontrol.core.tuner;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class MockTunerService implements TunerService {

  @Override
  public List<RadioStation> listStation() {
    return List.of(
        RadioStation.builder()
            .name("hr-xxl")
            .id("hr-xxl")
            .positionOnReceiver(StationPosition.of(3))
            .build());
  }
}
