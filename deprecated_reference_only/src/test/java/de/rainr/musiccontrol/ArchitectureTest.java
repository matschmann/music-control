package de.rainr.musiccontrol;


import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.Test;

class ArchitectureTest {

  @Test
  void core_does_not_depend_on_plugins() {
    JavaClasses importedClasses = new ClassFileImporter().importPackages("de.rainr.musiccontrol");

    ArchRule rule = noClasses().that().resideInAPackage("de.rainr.musiccontrol.core..")
        .should().dependOnClassesThat()
        .resideInAPackage("de.rainr.musiccontrol.plugins..");

    rule.check(importedClasses);
  }
}